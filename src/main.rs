// ver el warning, y lo que está dentro de paréntesis del warning, agregarlo a #![allow(xxx)]
#![allow(dead_code)]
use server::Server;
use website_handler::WebsiteHandler;
use std::env; // environment variable

mod http;
mod server;
mod website_handler;

// Probarlo con:
// echo "TEST" | netcat 127.0.0.1 8080
fn main() {
    // let public_path = env::var("PUBLIC_PATH").unwrap(); // Siempre hay que ponerle un env var, y
                                                        // eso es annoying
    //Pero Cargo nos da unas variables de entorno :)
    // default_path = env!("CARGO_MANIFEST_DIR"); // env! es macro para leer variables de entorno. Expande a esto:
                          // "/home/lyubo/workspace/server", verlo con cargo expand
                          // En mi caso es: default_path = "/home/karl/workspace/udemy/learn_rust_building_real_apps/server";
    let default_path = format!("{}/public", env!("CARGO_MANIFEST_DIR"));
    let public_path = env::var("PUBLIC_PATH").unwrap_or(default_path);
    println!("public path: {}", public_path);
    let server = Server::new("127.0.0.1:8080".to_string());
    server.run(WebsiteHandler::new(public_path));
}

// struct contiene la estructura a implementar, con sus atributos
//

/*
 * GET /user?id=10 HTTP/1.1\r\n
 * HEADERS \r\n
 * BODY
 */
