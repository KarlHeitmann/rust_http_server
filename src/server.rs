use crate::http::{ParseError, Request, Response, StatusCode}; // crate keyword significa la raiz del modulo
use std::convert::TryFrom; // Para poder usar el TryFrom implementado en Request, debo importarlo así también
use std::convert::TryInto; // ERASE_ME
use std::io::{Read, Write};
use std::net::TcpListener;

// One can implement default implementations for traits. Cool. Esto se hace ahora en
// handle_bad_request. Cualquiera puede sobreescribir ese trait, pero si no quiere, se va a correr
// eso.
pub trait Handler {
    fn handle_request(&mut self, request: &Request) -> Response;

    fn handle_bad_request(&mut self, e: &ParseError) -> Response {
        println!("Failed to parse request: {}", e);
        Response::new(StatusCode::BadRequest, None)
    }
}

pub struct Server {
    addr: String,
}

// impl contiene las implementaciones, las funciones/métodos que tendrá la estructura
impl Server {
    // self se representa a si mismo
    // // Two types: method, associated function.
    // methods: defined context of a struct. Always take first parameter "self"
    // associated function, son como las static.
    // fn new(addr: String) -> Server { //Equivalente al de abajo
    pub fn new(addr: String) -> Self { // Self con mayusculas es un alias para nombre struct
        Self {
            // addr: addr //
            addr // atajo para el de arriba
        }
    }

    pub fn run(self, mut handler: impl Handler) { // En otro caso, debiera de enviarse &self, o incluso
                   // ser una referencia mutable, porque con self solo al
                   // finalizar la función dealocaria self y la struct. Pero como run() va a correr para siempre,
                   // podemos omitir el

        println!("Listening on {}", self.addr);

        // Aqui están los Result. Tipos de errores en Rust: recoverable, unrecoverable.
        // Unrecoverable es que quiera acceder a indice de array inexistente, recoverable es que no
        // pueda leer un archivo. Por ejemplo.
        // Rust no tiene excepciones, el ocupa Result para eso:
        // pub enum Result<T, E> {
        //   Ok(T),
        //   Err(E),
        // }
        // unwrap() devuelve lo de Result. Si es OK, devuelve su resultado, si no es OK, hace panic
        let listener = TcpListener::bind(&self.addr).unwrap();

        // podria usarse while loop
        loop {

            // Ver SNIPPET_1
            match listener.accept() {
                // Ok(tup) => {
                // Ok(_) => {
                // Ok((stream, addr)) => {
                // Ok((stream, _)) => { // esto no funca, porque stream.read() requiere que sea mutable
                Ok((mut stream, _)) => {
                    // let mut buffer = [1,2,3,4];
                    let mut buffer = [0; 1024]; // esto crea un arreglo de ceros largo 1024
                    // que pasa si la solicitud tiene más de 1024? para producción habrá que ser
                    // más despiertos. Pero para este proyecto basta con largo 1024 es suficiente.
                    // Revisar https://doc.rust-lang.org/std/io/trait.Read.html#implementors para
                    // ver todas las partes donde se está implementando: para File, Empty, Stdin,
                    // TCPStream, etc. También se puede buscar Implementors para *write*
                    match stream.read(&mut buffer) { //.read es un trait, que hay que importar de std::io::Read;
                        Ok(_) => {
                            // println!("Received a request: {}", String::from_utf8());
                            println!("Received a request: {}", String::from_utf8_lossy(&buffer));
                            // TryFrom recibe &[u8], pero buffer es [0; 1024], hay que convertir el
                            // array de tamaño fijo a uno de tamaño variable. Una forma es la de
                            // abajo, pero la mejor es la que no va comentada
                            // Request::try_from(&buffer as &[u8]);
                            let response = match Request::try_from(&buffer[..]) { // crea el byte slice
                                Ok(request) => {
                                    handler.handle_request(&request)
                                    /*
                                    dbg!(request);
                                    // let response = Response::new(StatusCode::NotFound, None);
                                    let response = Response::new(
                                        StatusCode::Ok,
                                        Some("<h1> Hola Mundo </h1>".to_string())
                                    );
                                    response
                                    */
                                    // write!(stream, "HTTP/1.1 404 Not Found\r\n\r\n"); // XXX ESTA ES DE RIGHT MACRO para escribir algo al
                                    // response.send(&mut stream);
                                    /*
                                    write!(stream, "{}", response); // Se puede poner Response
                                                                    // directamente porque
                                                                    // implementa Display
                                     */
                                              // TCP Stream
                                }
                                Err(e) => handler.handle_bad_request(&e)
                                /*
                                Err(e) => {
                                    println!("Failed to parse a request: {}", e);
                                    Response::new(StatusCode::BadRequest, None)
                                }
                                */
                            };
                            if let Err(e) = response.send(&mut stream) {
                                println!("Failed to send response: {}", e);
                            }
                            // let res: &Result<Request, _> = &buffer[..].try_into(); // Esto se podría hacer en teoría
                        },
                        Err(e) => println!("Failed to read from connection: {}", e),
                    }
                },
                // _ => // esto es si no me importan los otros matches
                Err(e) => println!("Failed to establish a connection: {}", e),
            }
        }
    }
}

