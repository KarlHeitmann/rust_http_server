use std::fmt::{Display, Formatter, Result as FmtResult};

#[derive(Copy, Clone, Debug)] // Copy me va a pedir compiler also derive Clone. Debug es una buena
                              // práctica añadirlo siempre que se pueda.
pub enum StatusCode {
    Ok = 200,
    BadRequest = 400,
    NotFound = 404,
}

impl StatusCode {
    pub fn reason_phrase(&self) -> & str {
        match self {
            Self::Ok => "Ok",
            Self::BadRequest => "Bad Request",
            Self::NotFound => "Not Found",
        }
    }
}

//implement the Display trait
impl Display for StatusCode {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(f, "{}", *self as u16) // how do we cast it? Cuidado con castear la referencia en vez
                                     // del valor. Dereference. No implementa Copy trait. Por eso
                                     // tenemos que hacer algo mas. Cuidado con diferencia entre
                                     // Copy y Clone. derive Copy arriba
    }
}
