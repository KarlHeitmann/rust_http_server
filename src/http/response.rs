use std::net::TcpStream;
use std::io::{Write, Result as IoResult};
use std::fmt::{Display, Formatter, Result as FmtResult};

use super::StatusCode;

#[derive(Debug)]
pub struct Response {
    status_code: StatusCode,
    body: Option<String>,
}

impl Response {
    pub fn new(status_code: StatusCode, body: Option<String>) -> Self {
        Response { status_code, body }
    }
    // Queremos desacoplar el TcpStream para probarlo mejor. Le pasamos un trait como arg
    // Sabe que stream tiene el trait Write, pero no sabe la implementación. La implementación solo
    // estará en el TcpStream variable, o en el File variable, etc.
    // Es dyn porque se sabrá que implementación usar en runtime
    //
    // El de abajo es dinámico. En runtime se va a ir a una "V Table" donde están punteros a las
    // diferentes implementaciones que puede tomar. Y ahí se irá donde tiene que ir. Es poco, pero
    // puede ser un overhead considerable en la parte caliente de tu app. Abajo está static
    // implementation
    // pub fn send(&self, stream: &mut dyn Write) -> IoResult<()> {
    //
    // Static dispatch. Copiará esta función donde para todos los casos de diferentes llamados a
    // esta funcion. Por ejemplo creará además:
    // pub fn send_TcpStream(&self, stream: &mut TcpStream) -> IoResult<()> {
    // pub fn send_File(&self, stream: &mut File) -> IoResult<()> {
    // Drawback: compilation más lenta, binario final más grande
    pub fn send(&self, stream: &mut impl Write) -> IoResult<()> {
        let body = match &self.body {
            Some(b) => b,
            None => "",
        };

        write!(
            stream,
           "HTTP/1.1 {} {}\r\n\r\n{}",
           self.status_code,
           self.status_code.reason_phrase(),
           body
       )
    }
}

