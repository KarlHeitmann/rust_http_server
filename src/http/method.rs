use std::str::FromStr;

#[derive(Debug)]
pub enum Method {
    // GET(String), // los enum pueden contener data!!
    // DELETE(u64),
    GET, // los enum pueden contener data!!
    DELETE,
    POST,
    PUT,
    // PUT = 5, // Esto se puede hacer, asignarle un valor a un enum, y ese es el q tendra, los que le siguen incrementan ese value
    HEAD,
    CONNECT,
    OPTIONS,
    TRACE,
    PATCH,
}

impl FromStr for Method {
    type Err = MethodError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "GET" => Ok(Self::GET),
            "DELETE" => Ok(Self::DELETE),
            "POST" => Ok(Self::POST),
            "PUT" => Ok(Self::PUT),
            "HEAD" => Ok(Self::HEAD),
            "CONNECT" => Ok(Self::CONNECT),
            "OPTIONS" => Ok(Self::OPTIONS),
            "TRACE" => Ok(Self::TRACE),
            "PATCH" => Ok(Self::PATCH),
            _ => Err(MethodError)
        }
    }
}

pub struct MethodError;

