use super::method::{Method, MethodError};
use super::QueryString;
// std::convert -> lidia con conversiones
use std::convert::TryFrom; //TryFrom es un From que puede fallar
use std::error::Error;
use std::fmt::{Debug, Display, Formatter, Result as FmtResult}; // Para implementar el Display trait
use std::str;
use std::str::Utf8Error;

// derive: Rust compiprovide basic implementation for certain traits. Debug trait is one of thos,
// usar "derive". Cuando "#" va seguido por bang "!", aplica a TODO el módulo. No a lo que viene
// abajo.
#[derive(Debug)]
// Lifetime es un "tipo", por eso va con ángulos. Además, se nombran partiendo con commilla: 'a
// Este lifetime representa que la request está apuntando a otra memoria, ne nuestro caso el buffer
// de server. Como nuestro Request es genérico, debemos darle un lifetime en el bloque de impl más
// abajo. Compilador se quejará en QueryString, y en Method. Usar derive(Debug) en los lugares
// donde alega.
pub struct Request<'buf> {
    path: &'buf str,
    query_string: Option<QueryString<'buf>>, // Esto es una opcion, puede ser o algun string, o un None
    method: Method,
    // method: super::method::Method, // super se va al padre, y busca modulo method, funciona sin el use de arriba
}

impl<'buf> Request<'buf> {
    // Getter: convention is to name it after the field without prefixing "get" word to it.
    pub fn path(&self) -> &str {
        &self.path
    }
    pub fn method(&self) -> &Method {
        &self.method
    }

    // Im not interested in Option, but in it's content
    pub fn query_String(&self) -> Option<&QueryString> {
        // as_ref: converts from &Option<T> to Option<&T>
        // a reference of a option of a T, to a an option of a reference to a T.
        self.query_string.as_ref()
    }
}

// Tenemos que darle un tipo al TryFrom, por eso el <&[u8]>
// Al implementar TryFrom, el compilador sabrá como implementar TryInto, la operación reversa del
// TryFrom. Eso es lo que dice la documentación doc.rust-lang.org/std/convert/trait.TryInto.html
// Sobre Lifetime: Tenemos que definir el lifetime buf justo dp del impl. Quedando Request<'buf> {.
// Ahí lo puedo usar.
impl<'buf> TryFrom<&'buf [u8]> for Request<'buf> {
    type Error = ParseError; // Hay que poner un alias, asignarle algo a Error, porque lo exige std convert tryfrom
    
    // GET /search?name=abc&sort=1 HTTP/1.1\r\n..HEADERS...
    fn try_from(buf: &'buf [u8]) -> Result<Request<'buf>, Self::Error> {
        let request = str::from_utf8(buf)?;

        // SNIPPET_5

        // ok_or transforma opcion en result
        // request está haciendo shadow del previous request, funciona.
        let (method, request) = get_next_word(request).ok_or(ParseError::InvalidRequest)?;
        let (mut path, request) = get_next_word(request).ok_or(ParseError::InvalidRequest)?;
        let (protocol, _) = get_next_word(request).ok_or(ParseError::InvalidRequest)?;

        if protocol != "HTTP/1.1" {
            return Err(ParseError::InvalidProtocol);
        }
        // Para que funcione ?, debo de implementar impl From<MethodErr> for ParseError.
        // Porque este lugar tiene el ParseError definidodsa
        // Compilador dice "the trait `From<MethodError>` is not implemented for `ParseError`
        // como está implementado "FromStr" en archivo method.rs para los Method, al especificar la
        // variable method que es de tipo Method, y al ser method un string, puedo usar
        // method.parse. Interrogación es por lo que va arriba de este comentario
        let method: Method = method.parse()?;

        let mut query_string = None;
        if let Some(i) = path.find('?') {
            query_string = Some(QueryString::from(&path[i + 1..]));
            // query_string = Some(&path[i + 1..]);
            path = &path[..i];
        }

        Ok(Self {
            path: path,
            query_string,
            method,
        })
    }
}

fn get_next_word(request: &str) -> Option<(&str, &str)> {
    // SNIPPET_4

    // for c in request.chars() {
    for (i, c) in request.chars().enumerate() {
        if c == ' ' || c == '\r'{
            return Some((&request[..i], &request[i + 1..])); // Riesgoso porque puede out of bound. Pero dentro del marco que espacio le sigue a palabra es seguro
        }
    }

    None
}

pub enum ParseError {
    InvalidRequest,
    InvalidEncoding,
    InvalidProtocol,
    InvalidMethod,
}

impl ParseError {
    fn message(&self) -> &str {
        match self {
            Self::InvalidRequest => "Invalid Request",
            Self::InvalidEncoding => "Invalid Encoding",
            Self::InvalidProtocol => "Invalid Protocol",
            Self::InvalidMethod => "Invalid Method",
        }
    }
}

impl From<MethodError> for ParseError {
    fn from(_: MethodError) -> Self {
        Self::InvalidMethod
    }
}

impl From<Utf8Error> for ParseError {
    fn from(_: Utf8Error) -> Self {
        Self::InvalidEncoding
    }
}

// TIP: Ir a la documentación de Display, y copy paste esto:
// fn fmt(&self, f: &mut Formatter<'_>) -> Result;
// y modificarlo
impl Display for ParseError {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(f, "{}", self.message())
    }
}

impl Debug for ParseError {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(f, "{}", self.message())
    }
}
/*
pub trait Error: Debug + Display {
    /// The lower-level source of this error, if any.
  Esto en la documentacion de Error (<C-d>) indica que trait Error solo puede ser implementado en types que ya tengan
  definido lo que es trait Debug y Display

  Display debug trait
  Display se usa para formatear un string, en println!("{}", algo) por ejemplo.
  Debug se usa para formatear un string, en println!("{:?}", algo) por ejemplo. Cuando quiero dar más info en println!
 */
impl Error for ParseError {
}

