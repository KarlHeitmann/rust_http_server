use std::collections::HashMap;

// ejemplo
// Interesting: a, b are OK. c strange. d array ["", "7", "abc"]

#[derive(Debug)]
pub struct QueryString<'buf> {
    data: HashMap<&'buf str, Value<'buf>> // dos tipos, de key y value
}

#[derive(Debug)]
pub enum Value<'buf> {
    Single(&'buf str),
    Multiple(Vec<&'buf str>), // heap allocated array, Vec
}

impl<'buf> QueryString<'buf> {
    pub fn get(&self, key: &str) -> Option<&Value> {
        self.data.get(key)
    }
}

// No se puede usar FromStr porque no contiene un lifetime parameter. Solo puede parsear types que
// no contienen lifetime parameter. Hay que usar From.
// Implementamos From y no TryFrom, porque esto _no puede fallar_ Cualquier string pasado debe
// enviar algo.
// a=1&b=2&c&d=&e===&d=7&d=abc
impl<'buf> From<&'buf str> for QueryString<'buf> {
    fn from(s: &'buf str) -> Self {
        let mut data = HashMap::new();

        for sub_str in s.split('&') {
            let mut key = sub_str;
            let mut val = "";

            // s.find devuelve un Option, y si el resultado es some, entonces se mete.
            // Some(i) = s.find('=') es la expresión que evaluará si es True. Si es trur, s.find
            // devuelve Some(i) y no None, ejecutará el código entre bloques, creando una variable
            // "i" que tomará el índice encontrado
            if let Some(i) = sub_str.find('=') {
                key =  &sub_str[..i];
                val =  &sub_str[i + 1..];
            }

            data.entry(key)
                // puede ser así .and_modify(|existing| {}) o  como abajo
                .and_modify(|existing: &mut Value| match existing {
                    Value::Single(prev_val) => {
                        // let mut vec = vec![prev_val, val];
                        // Esto falla, porque existing es un PUNTERO, hay que dereferenciarlo
                        // porque antes era Single, y ahora hay que asignar un nuevo
                        // Value::Multiple. Esto funciona porque en un ENUM, está hecho para que
                        // cada posibilidad ocupe la misma cantidad de memoria ram.
                        // existing = Value::Multiple(vec![prev_val, val]);
                        *existing = Value::Multiple(vec![prev_val, val]);
                        /*
                        let mut vec = Vec::new();
                        vec.push(val);
                        vec.push(prev_val);
                        */
                    }
                    Value::Multiple(vec) => vec.push(val)
                })
                .or_insert(Value::Single(val));

        }

        QueryString { data }
    }
}

