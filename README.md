Desde standard library, el null se puede representar en Rust como un Option:

pub enum Option<T> {
  None,
  Some(T),
}

Como puedes ver, un Option puede ser ya sea None, o Some de algun tipo. Es un genérico de alguna opción
```rust
fn main() {
    let string = String::from("127.0.0.1:8080");
    let string_slice = &string[10..]; // <--- generalmente no haremos esto, en rust se usa UTF-8,
                                      // give me everything after 10 bytes
    let string_borrow: &str = &string;
    let string_literal = "1234";

    dbg!(&string);
    dbg!(string_slice);
    dbg!(string_borrow);
    dbg!(string_literal);
    // let server = Server::new("127.0.0.1:8080");
    // server.run();
}

```

loops

```
'outer: loop {
    loop {
        break 'outer;
        continuer 'outer;
    }
}
```
SNIPPET_1
```
            // .accept devuelve -> io::Result<(TcpStream, SocketAddr)>
            let res = listener.accept(); // el codigo se cuelga aqui hasta que recibe una conexion

            if res.is_err() { continue; }

            let (stream, addr) = res.unwrap(); // devuelve un tuple
```
Esto es equivalente al match
```
            match listener.accept() {
                // Ok(tup) => {
                // Ok(_) => {
                // Ok((stream, _)) => {
                Ok((stream, addr)) => {
                    let a = 5;
                    println!("OK");
                },
                // _ => // esto es si no me importan los otros matches
                Err(e) => println!("Failed to establish a connection: {}", e),
            }
```
match puede usarse como switch:
```
            match "abcd" {
                "abcd" => println!(),
                "a" | "b" => {},
                _ => {}

            }
```


SNIPPET_2
Son equivalentes los siguientes snippets:
```
        match str::from_utf8(buf) {
            Ok(request) => {},
            Err(_) => return Err(ParseError::InvalidEncoding),
        }

```

```
        // DOC or:
        // Returns `res` if the result is [`Err`], otherwise returns the [`Ok`] value of `self`.
        //
        // Arguments passed to `or` are eagerly evaluated; if you are passing the
        // result of a function call, it is recommended to use [`or_else`], which is
        // lazily evaluated.
        match str::from_utf8(buf).or(Err(ParseError::InvalidEncoding)) {
            Ok(request) => {},
            Err(e) => return Err(e),
        }
```

```
        //str::from_utf8(buf).or(Err(ParseError::InvalidEncoding))?;
```

```
        // Ayudarse del compilador para ver que trait implementar
        str::from_utf8(buf)?

        [...]

impl From<Utf8Error> for ParseError {
    fn from(_: Utf8Error) -> Self {
        Self::InvalidEncoding
    }
}

```

SNIPPET_4
Equivalentes:

```
    let mut iter = request.chars();
    loop {
        let item = iter.next();
        match item {
            Some(c) => {},
            None => break,
        }
    }
```
```
    for c in request.chars() {

    }
```

SNIPPET_5
EQUIVALENTES
```
        match get_next_word(request) {
            Some((method, request)) => {},
            None => return Err(ParseError::InvalidRequest),
        }
```
```

        // ok_or transforma opcion en result
        let (method, request) = get_next_word(request).ok_or(ParseError::InvalidRequest);
```

SNIPPET_6
EQUIVALENTES
```
        let mut query_string = None;
```
```
        match path.find('?') {
            Some(i) => {
                query_string = Some(&path[i + 1..]); // OJO el + 1 es para no tomar '?', esto funciona porque '?' es 1 byte. Si fuera un string UTF-8 no se puede hacer esto
                path = &path[..i];
            },
            None => {}
        }
```
```

        let q = path.find('?');
        if q.is_some() {
            let i = q.unwrap();
            query_string = Some(&path[i + 1..]);
            path = &path[..i];
        }
```
```

        if let Some(i) = path.find('?') {
            query_string = Some(&path[i + 1..]);
            path = &path[..i];
        }
```


